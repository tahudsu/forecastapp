# README #

### What is this repository for? ###

* Forecast app to learn react-redux
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* git clone https://tahudsu@bitbucket.org/tahudsu/forecastapp.git
* npm install
* npm start - to run server on http://localhost:8080/webpack-dev-server/

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* tahudsu@gmail.com || jmezquerraf@hotmail.com
